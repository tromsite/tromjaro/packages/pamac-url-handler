# pamac URL handler package

Install ALPM packages with `pamac-installer` from URL scheme `x-alpm-package://[aur/]<packagename>`

## Building the package

Open Terminal and issue the command:

`make`

To install the package right after the building, use:

`make install`

To remove just installed package, use:

`make remove`

To cleanup the project, use:

`make clean`

