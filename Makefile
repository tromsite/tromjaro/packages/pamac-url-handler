# makefile for pamac-url-handler
build:
	makepkg

install:
	pamac-installer pamac-url-handler-*.tar.xz

remove:
	pamac-installer --remove pamac-url-handler

clean:
	$(RM) -rf src/ pkg/ *.tar.xz
